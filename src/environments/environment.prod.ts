export const environment = {
  production: true,
  kittenApiUrl: 'http://samati.xyz/assets/cats.json',
  backendUrl: 'http://samati.xyz:7070',
};
