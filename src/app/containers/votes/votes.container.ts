import { Page } from './../../models/page';
import { VoteService } from './../../services/vote.service';
import { Component, OnInit } from '@angular/core';
import { Vote } from 'src/app/models/vote';

@Component({
  selector: 'kt-votes',
  templateUrl: './votes.container.html',
})
export class VotesContainer implements OnInit {
  currentPage: Page<Vote>;

  constructor(private voteService: VoteService) {
    this.loadNextPage();
  }

  ngOnInit(): void {}

  loadNextPage() {
    this.voteService.getVotes().subscribe((page) => {
      this.currentPage = page;
    });
  }
}
