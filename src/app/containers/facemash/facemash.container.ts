import { VoteService } from './../../services/vote.service';
import { LoginFormComponent } from './../../components/login-form/login-form.component';
import { AuthService } from './../../services/auth.service';
import { Observable } from 'rxjs';
import { KittenPaire, Kitten } from './../../models/kitten';
import { KittenService } from './../../services/kitten.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

@Component({
  selector: 'kt-facemash',
  templateUrl: './facemash.container.html',
  styleUrls: ['./facemash.container.scss'],
})
export class FaceMashContainer implements OnInit {
  kittenPaire$: Observable<KittenPaire>;
  activateRight: boolean;
  activateLeft: boolean;

  constructor(
    private kittenService: KittenService,
    private authService: AuthService,
    private dialog: MatDialog,
    private voteService: VoteService
  ) {}

  ngOnInit(): void {
    this.kittenPaire$ = this.kittenService.getNext();
  }

  onVote(kitten: Kitten, side: string) {
    console.log(side);
    if (this.authService.isAuthentificate()) {
      setTimeout(() => {
        if (side === 'right') {
          this.activateLeft = true;
        } else if (side === 'left') {
          this.activateRight = true;
        }
        setTimeout(() => {
          this.voteService.vote(kitten).subscribe((_) => {
            this.kittenPaire$ = this.kittenService.getNext();
          });
        }, 1000);
      }, 500);
    } else {
      this.showformAuthentification();
    }
    this.activateLeft = false;
    this.activateRight = false;
  }

  showformAuthentification() {
    const dialogRef = this.dialog.open(LoginFormComponent, {
      width: '00px',
      data: { name: 'mm', animal: '' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }


}
