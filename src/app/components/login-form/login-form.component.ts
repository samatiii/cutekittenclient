import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'kt-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  username: string;
  password: string;
  firstname: string;
  lastname: string;

  mode = 1;
  hasSignupError: boolean;

  constructor(
    private authService: AuthService,
    public dialogRef: MatDialogRef<LoginFormComponent>
  ) {}

  ngOnInit(): void {}

  signin() {
    console.log('uuuu');
    this.authService
      .signin({
        username: this.username,
        password: this.password,
      })
      .subscribe(() => {
        this.closeModal();
      });
  }

  signup() {
    this.authService
      .signup({
        username: this.username,
        password: this.password,
        firstname: this.firstname,
        lastname: this.lastname,
      })
      .subscribe(
        () => {
          this.signupOnSuccess();
          this.mode = 1;
        },
        (err) => {
          this.hasSignupError = true;
        }
      );
  }
  signupOnSuccess() {}

  private closeModal() {
    this.dialogRef.close();
  }
}
