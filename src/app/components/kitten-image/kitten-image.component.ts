import { Kitten } from './../../models/kitten';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'kt-kitten-image',
  templateUrl: './kitten-image.component.html',
  styleUrls: ['./kitten-image.component.scss'],
})
export class KittenImageComponent implements OnInit {
  @Input() kitten: Kitten;
  @Output() onVote = new EventEmitter<Kitten>();

  constructor() {}

  ngOnInit(): void {}

  vote() {
    this.onVote.next(this.kitten);
  }
}
