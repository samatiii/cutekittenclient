import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KittenImageComponent } from './kitten-image.component';

describe('KittenImageComponent', () => {
  let component: KittenImageComponent;
  let fixture: ComponentFixture<KittenImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KittenImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KittenImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
