import { Vote } from './../../models/vote';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'kt-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss'],
})
export class VoteComponent implements OnInit {
  @Input()
  vote: Vote;

  constructor() {}

  ngOnInit(): void {}
}
