import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-view-switcher',
  templateUrl: './view-switcher.component.html',
  styleUrls: ['./view-switcher.component.scss'],
})
export class ViewSwitcherComponent implements OnInit {

@Input() target: string;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateTo() {
    this.router.navigateByUrl(this.target);
  }

  logOut(): void{
    console.log(' befor '+    localStorage.getItem('token'))
    localStorage.removeItem('token');
    console.log(' after '+    localStorage.getItem('token'))
  }
}
