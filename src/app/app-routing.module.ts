import { VotesContainer } from './containers/votes/votes.container';
import { FaceMashContainer } from './containers/facemash/facemash.container';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'facemash',
    component: FaceMashContainer,
  },

  {
    path: 'votes',
    component: VotesContainer,
  },

  { path: '**', redirectTo: 'facemash', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
