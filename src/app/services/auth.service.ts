import { tap } from 'rxjs/operators';
import { SignupRequest } from './../models/signup-request';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private signupUrl = environment.backendUrl + '/users/signup';
  private signinUrl = environment.backendUrl + '/users/signin';

  private authenticated = false;
  private token: string;

  constructor(private http: HttpClient) {
    const accessToken = localStorage.getItem('token');
    if (!!accessToken) {
      this.authenticated = true;
      this.token = accessToken;
    }
  }

  isAuthentificate() {
    if(!!localStorage.getItem('token')){
      return this.authenticated;
    }else{
      return false;
    }
  }

  signup(request: SignupRequest) {
    return this.http.post(this.signupUrl, request);
  }

  signin(request: { username: string; password: string }) {
    return this.http.post(this.signinUrl, request).pipe(
      tap((data: any) => {
        this.persistUserData(data);
        this.authenticated = true;
        this.token = data.accessToken;
      })
    );
  }
  persistUserData(data) {
    localStorage.setItem('token', data.accessToken);
  }

  getToken() {
    return this.token;
  }
}
