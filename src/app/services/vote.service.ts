import { Page } from './../models/page';
import { Vote } from './../models/vote';
import { Observable } from 'rxjs';
import { Kitten } from './../models/kitten';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VoteService {
  private voteUrl = environment.backendUrl + '/votes';

  constructor(private http: HttpClient) {}

  public vote(kitten: Kitten) {
    return this.http.post(this.voteUrl, {
      kittenId: kitten.id,
      kittenUrl: kitten.url,
    });
  }

  public getVotes(): Observable<Page<Vote>> {
    return this.http.get<Page<Vote>>(this.voteUrl);
  }
}
