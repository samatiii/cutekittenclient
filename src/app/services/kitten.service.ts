import { KittenPaire, Kitten } from './../models/kitten';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of as ObservalbeOf } from 'rxjs';
import { tap, map } from 'rxjs/operators';

export function shuffle(array) {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

@Injectable({
  providedIn: 'root',
})
export class KittenService {
  kittens: Kitten[];
  constructor(private http: HttpClient) {}

  private initializeKittens() {
    this.http.get<Kitten[]>("assets/cats.json").subscribe((kittens) => {
      this.kittens = kittens;
    });
  }

  getNext(): Observable<KittenPaire> {
    if (this.kittens == null) {
      return this.http.get<{ images: Kitten[] }>("assets/cats.json").pipe(
        map((data) => data.images),
        tap((kittens) => {
          this.kittens = kittens;
        }),
        map((kittens) => this.randomPaire(kittens))
      );
    } else {
      return ObservalbeOf(this.randomPaire(this.kittens));
    }
  }
  randomPaire(kittens: Kitten[]): KittenPaire {
    kittens = shuffle(kittens);
    return { first: kittens[0], second: kittens[1] };
  }
}
