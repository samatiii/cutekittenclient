import { VotesContainer } from './containers/votes/votes.container';
import { TokenInterceptor } from './interceptors/jwt-interceptor';
import { FaceMashContainer } from './containers/facemash/facemash.container';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { KittenImageComponent } from './components/kitten-image/kitten-image.component';
import { ViewSwitcherComponent } from './components/view-switcher/view-switcher.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { VoteComponent } from './components/vote/vote.component';
import { MatCardModule } from '@angular/material/card';

const CONTAINERS = [FaceMashContainer, VotesContainer];

@NgModule({
  declarations: [
    AppComponent,
    ...CONTAINERS,
    KittenImageComponent,
    ViewSwitcherComponent,
    LoginFormComponent,
    VoteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [LoginFormComponent],
})
export class AppModule {}
