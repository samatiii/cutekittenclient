import { Kitten } from './kitten';
export interface Vote {
  id?: number;
  kitten: Kitten;
  voteCount: number;
}
