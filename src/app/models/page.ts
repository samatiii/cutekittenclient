export interface Page<T> {
  content: T[];
  pageNumber: number;
  pageSize: number;
  offset: number;
  totalElements: number;
  totalPages: number;
  numberOfElements: number;
}
