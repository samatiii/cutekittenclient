export interface Kitten {
  id: string;
  url: string;
}

export interface KittenPaire {
  first: Kitten;
  second: Kitten;
}
